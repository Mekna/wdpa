#!/usr/bin/python
# -*- coding: utf-8 -*-
from random import randint
import random

print("wdpa 0.1")
print("simple password generator by kaskade")

leave = False
opts={'a':'letters','n':'numbers', 's':'symbols', 'x':'specials', 'p':'pronouncable', 'w':'whitespace',
'l':'lowercase only', 'u':'uppercase only'}
cat={'a':'abcdefghijklmnopqrstuvwxyz', 'n':'0123456789', 's':'/*-#{([])}@+=_><:!?;%|\"\'', 
'x':'~^çàéè&"£$µ', 'w':' '}
default_opts=['a','n','s',12]

def parse(s):
	if s.startswith('q'):
		return -2
	elif not s or s[-1].isalpha():
		return -1

	select = []
	for char in s:
		if char.isalpha():
			if char in opts:
				print(opts[char] + " true")
				select.append(char)
		elif char.isdigit():
			splt=s.partition(char)
			#print("split:"+splt[0]+","+splt[1]+","+splt[2])
			slength=''.join((splt[1],splt[2]))
			if not slength.isdigit():
				return -1
			print("length "+slength)
			select.append(int(slength))
			return select
	return -1

def generate(options):
	#making sure valid options or setting default
	if not options:
		options=default_opts
	elif len(options) == 1:
		if isinstance(options[0], int):
			length=options[0]
			options=default_opts
			options[-1]=length
		elif options[0].isdigit():
			length=int(options[0])
			options=default_opts
			options[-1]=length
		else:
			options=defaults_options

	#setting modifiers and removing from the list
	pronounce=False; lower=False; upper=False;
	if 'p' in options:
		pronounce=True
		options.remove('p')
	if 'l' in options:
		lower=True
		options.remove('l')
	if 'u' in options:
		upper=True
		options.remove('u')

	#removing length
	length = options[-1]; del options[-1];

	pwd=[]
	for i in range(length):
		ra=randint(0, len(options)-1)
		chars=list(cat[options[ra]])
		random.shuffle(chars)
		if upper:
			pwd.append(chars[0].upper())
		elif not upper and not lower:
			raa=randint(0,1)
			if raa==0:
				pwd.append(chars[0].upper())
			else:
				pwd.append(chars[0].lower())

	return ''.join(pwd)

while not leave:
	print("\n\nOPTIONS")
	print("a:letters\tn:numbers\ts:symbols\tx:specials")
	print("p:pronouncable\tw:whitespace\tl:lowercase\tu:uppercase")
	print("Ex: answl12, apu8")
	stropts=raw_input("\nSet your options :\n");
	usropts=parse(stropts)
	if usropts==-1:
		print("! bad input")
		leave=True
		break
	elif usropts==-2:
		print("! leaving")
		leave=True
		break

	pd=generate(usropts)
	print("Password : "+ pd)

print("\nbye")
